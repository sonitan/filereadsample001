package com.c0113212.filereadsample001;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class MainActivity extends AppCompatActivity implements Runnable{
    //    自分のmacアドレス情報
    BluetoothAdapter mybluetoothAdapter01 =
            BluetoothAdapter.getDefaultAdapter();
    //ペアリングされていない端末の情報格納(ListViewにつかう)
    ArrayList<String> btname01 = new ArrayList<String>();
    //ペアリングされてない端末の情報格納(BluetoothClientに使う)
    ArrayList<BluetoothDevice> btinfor01 = new ArrayList<BluetoothDevice>();
    //Bluetooth対応を調べる
    private static final int REQUEST_ENABLE_BLUETOOTH = 0;
    //Downloadに格納しているファイル名
    String FileName[]={"/sample.txt","/sample (1).txt","/sample (2).txt","/sample (3).txt",
            "/sample (4).txt","/sample (5).txt","/sample (6).txt","/sample (7).txt","/sample (8).txt"
            ,"/sample (9).txt","/sample (10).txt"};
    //Androidローカルに保存
    private static final String FILE_NAME = "FileSampleFile";
    Thread thread;
    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //NanoHttpd起動
        Button nStar = (Button)findViewById(R.id.N_Start);
        nStar.setOnClickListener(new StartButtonClickListener01());
        //NanoHttpd停止
        Button nstop = (Button)findViewById(R.id.N_Stop);
        nstop.setOnClickListener(new StartButtonClickListener02());
        //Bluetooth端末表示許可
        Button bFind = (Button)findViewById(R.id.B_Find);
        bFind.setOnClickListener(new StartButtonClickListener03());
        //Bluetoothペアリングを開始
        Button bPear = (Button)findViewById(R.id.B_Pearing);
        bPear.setOnClickListener(new StartButtonClickListener04());
        //ローカルファイルに保存
        Button fRead = (Button)findViewById(R.id.F_Read);
        fRead.setOnClickListener(new StartButtonClickListener05());
        //ローカルファイル表示
        Button fDelete = (Button)findViewById(R.id.F_Show);
        fDelete.setOnClickListener(new StartButtonClickListener06());
        //ローカルファイル表示
        Button fClear = (Button)findViewById(R.id.F_Delete);
        fClear.setOnClickListener(new StartButtonClickListener07());
        //ダウンロード数を調べる
        Button fCheck = (Button)findViewById(R.id.F_Check);
        fCheck.setOnClickListener(new StartButtonClickListener08());
        ListView listView01 = (ListView) findViewById(R.id.btname01);

        //ペアリング端末を選択した時のLISTVIEW動作
        listView01.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //第一引数はBluetoothDevice型の接続先　第二は自分のBluetootjAdapte方のデバイス
                    BluetoothClientThread BtClientThread = new BluetoothClientThread(btinfor01.get(position), mybluetoothAdapter01);
                    BtClientThread.start();
            }
        });
    }

    //NanoHTTPD起動
    class StartButtonClickListener01 implements View.OnClickListener {
        public void onClick(View v) {
            TextView tv = (TextView)findViewById(R.id.NanoHttpdCheck);
            //確認だいあろぐ
            tv.setText("起動しています");
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("NanoHttpd")
                    .setMessage("起動しました")
                    .setPositiveButton("OK", null)
                    .show();
            try {
              WebServer wb =new WebServer();
                wb.start();
            } catch (IOException e) {
            }
        }
    }

    //NanoHTTPD停止
    class StartButtonClickListener02 implements View.OnClickListener {
        public void onClick(View v) {

        }
    }

    //Bluetooh端末検索許可
    class StartButtonClickListener03 implements View.OnClickListener {
        public void onClick(View v) {
            //            Bluetoothペアリングの検索を可能にする
            Log.d("chk100", "捜索許可だすのんな");
            if (mybluetoothAdapter01.getScanMode() !=
                    BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
                Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
                discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
                startActivity(discoverableIntent);
            }
        }
    }

    //周辺のペアリングしていない端末を調べますの
    class StartButtonClickListener04 implements View.OnClickListener {
        public void onClick(View v) {
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setTitle("Bluetooth情報");
            progressDialog.setMessage("取得中");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
            thread = new Thread(MainActivity.this);
            thread.start();
            Set<BluetoothDevice> pairedDevices = mybluetoothAdapter01.getBondedDevices();
            // Indicate scanning in the title
            // If we're already discovering, stop it
            if (mybluetoothAdapter01.isDiscovering()) {
                mybluetoothAdapter01.cancelDiscovery();
            }
            // Request discover from BluetoothAdapter
            mybluetoothAdapter01.startDiscovery();
        }
    }

    //ローカルファイルに書き込み
    class StartButtonClickListener05 implements View.OnClickListener {
        public void onClick(View v) {
            String s;
            BufferedInputStream istream;

            //DownloadPath
            File file05 = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            //ファイル個数分書き込む
            for (int i =0; i<FileName.length-1; i++) {
                //Fileのpath
                File path = new File(file05.getPath()+FileName[i]);
                //Fileが存在するか調べる
                if (path.exists()) {
                    Log.d("ファイル", "あります");
                    // Write
                    {
                        try {
                            FileOutputStream stream
                                    = openFileOutput(FILE_NAME, MODE_APPEND);
                            BufferedWriter out
                                    = new BufferedWriter(new OutputStreamWriter(stream));

                            istream = new BufferedInputStream(new FileInputStream(path));
                            byte[] buffer = new byte[1000];
                            istream.read(buffer);
                            s = new String(buffer).trim(); // 余分なデータを消去
                            istream.close();
                            Log.d("文字読み込み", s);
                            out.write(s);
                            out.newLine();
                            out.close();
                            path.delete();
                        } catch (Exception e) {
                        }
                    }
                }
                else{
                    Log.d("ファイル"," ないです");
                }
            }
        }
    }

    //ローカルファイルを表示
    class StartButtonClickListener06 implements View.OnClickListener {
        public void onClick(View v) {
            // メッセージ表示用
            String str  = "";
            TextView label = (TextView)findViewById(R.id.tv_message);
                // ファイルからデータ取得
            label.setText("");
                try{
                    FileInputStream stream
                            = openFileInput(FILE_NAME);
                    BufferedReader in
                            = new BufferedReader(new InputStreamReader(stream));

                    String line = "";
                    while((line = in.readLine())!=null){
                        str += line + "\n";
                    }
                    in.close();
                }catch(Exception e){
                    str  = "データ取得に失敗しました！";
                }
            // メッセージ表示
            label.setText(str);
        }
    }

    //ローカルファイルを削除
    class StartButtonClickListener07 implements View.OnClickListener {
        public void onClick(View v) {
            // ファイルのデータ削除
            //Yes,Cancelで選択可能にする
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("File情報")
                    .setMessage("保存された内容を削除しますか？")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // OK button pressed
                            String tmp  = "";
                            TextView label02 = (TextView)findViewById(R.id.tv_message);
                            try{
                                deleteFile(FILE_NAME);
                                tmp = "削除しました！";
                                label02.setText(tmp);
                            }catch(Exception e){
                                tmp  = "データ削除に失敗しました！";
                                label02.setText(tmp);
                            }
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();
        }
    }

    //ファイル個数を調べる
    class StartButtonClickListener08 implements View.OnClickListener {
        public void onClick(View v) {
            int filenumber=0;
            TextView FileNumber = (TextView)findViewById(R.id.FileNumber);
            //DownloadPath
            File file05 = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

            for (int i =0; i<FileName.length-1; i++) {
                //Fileのpath
                File path = new File(file05.getPath()+FileName[i]);
                //Fileが存在するか調べる
                if (path.exists()) {
                    Log.d("ファイル", "あります");
                    // Write
                    filenumber++;
                }
                else{
                    Log.d("ファイル"," ないです");
                }
            }
            FileNumber.setText(filenumber+")");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        //ダウンロードにあるファイル数を調べる
        int filenumber=0;
        //ファイル数を表示する
        TextView FileNumber = (TextView)findViewById(R.id.FileNumber);
        //DownloadPath
        File file05 = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

        for (int i =0; i<FileName.length-1; i++) {
            //Fileのpath
            File path = new File(file05.getPath()+FileName[i]);
            //Fileが存在するか調べる
            if (path.exists()) {
                Log.d("ファイル", "あります");
                // Write
                filenumber++;
            }
            else{
                Log.d("ファイル"," ないです");
            }
        }
        //ファイル数を表示する
        FileNumber.setText(filenumber + ")");

//        //Bluetoothが対応及びONになっているかをしらべる
//        if (mybluetoothAdapter01 == null) {
//            //端末がBluetoothに対応しているか調べるnullは対応していない
//            Log.d("chk55", "この端末はBluetoothに対応してないよ（白目）");
//        } else {//対応してたら
//            Log.d("chk57", "この端末はBluetoothに対応している（確信）");
////            端末のBluetooth設定がOFFの場合
//            if (!mybluetoothAdapter01.isEnabled()) {
//                Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
//                startActivityForResult(intent, REQUEST_ENABLE_BLUETOOTH);
//                return;
//            }
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        検索開始後リモートデバイスを検知した時の処理
        IntentFilter filter = new IntentFilter();
//        デバイス検索終了でブロードキャストされる
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
//        デバイスの検索を開始した時にブロードキャスト
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
//        デバイス検知の時点でブロードキャスト
        filter.addAction(BluetoothDevice.ACTION_FOUND);
//　　　　　デバイス名変更時にブロードキャスト
        filter.addAction(BluetoothDevice.ACTION_NAME_CHANGED);
        registerReceiver(DevieFoundReceiver, filter);
    }

    //    周辺のデバイス情報を検索する
    private final BroadcastReceiver DevieFoundReceiver = new BroadcastReceiver() {
        //検出されたデバイスからのブロードキャストを受ける
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            String dName = null;
            BluetoothDevice foundDevice;
            if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
                Log.d("d", "スキャン開始");
            }
            // デバイス名変更の場合
            if (BluetoothDevice.ACTION_NAME_CHANGED.equals(action)) {
                // デバイスを表示用アダプターに設定映
                Log.d("d", "デバイス名変更");
            }
            // デバイス発見の場合
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // デバイスを表示用アダプターに設定
                Log.d("d", "デバイス発見");
                foundDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if ((dName = foundDevice.getName()) != null) {
                    if (foundDevice.getBondState() != BluetoothDevice.BOND_BONDED) {
                        //接続したことのないデバイスのみアダプタに詰める
                        Log.d("デバイス名", dName);
//                        Listに追加
                        btname01.add(dName);
//                        Bluetooth型にデバイス情報かくんのう
                        btinfor01.add(foundDevice);
                        Log.d("デバイス格納数", String.valueOf(btname01.size()));
                    }
                }
            }
            if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                Log.d("d", "スキャン終了");
                ArrayAdapter<String> adapter01 = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, btname01);
                ListView listView01 = (ListView) findViewById(R.id.btname01);
                listView01.setAdapter(adapter01);
            }
        }
    };

    public class BluetoothClientThread extends Thread {
        //クライアント側の処理
        private final BluetoothSocket clientSocket;
        //        mDeviceは接続する相手の情報を入れる
        private final BluetoothDevice mDevice;
        byte[] sendmsg;

        //UUIDの生成
        public final UUID TECHBOOSTER_BTSAMPLE_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
        //        自分のデバイス情報
        BluetoothAdapter myClientAdapter;

        //コンストラクタ定義
        public BluetoothClientThread(BluetoothDevice device, BluetoothAdapter btAdapter) {
            //各種初期化
            BluetoothSocket tmpSock = null;
//            deviceにはListviewで選択した端末情報情報が入っている
            mDevice = device;
//            btAdapterは自分のデバイスの端末情報が入っている
            myClientAdapter = btAdapter;

            try {
                //自デバイスのBluetoothクライアントソケットの取得
                tmpSock = device.createRfcommSocketToServiceRecord(TECHBOOSTER_BTSAMPLE_UUID);
            } catch (IOException e) {
                e.printStackTrace();
            }
            clientSocket = tmpSock;
        }

        public void run() {
            //接続要求を出す前に、検索処理を中断する。
            if (myClientAdapter.isDiscovering()) {
                myClientAdapter.cancelDiscovery();
            }
            try {
                //サーバー側に接続要求
                clientSocket.connect();
            } catch (IOException e) {
                try {
                    clientSocket.close();
                } catch (IOException closeException) {
                    e.printStackTrace();
                }
                return;
            }
        }
        public void cancel() {
            try {
                clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void run() {
//       Log.d("呼び出されては","いるんですよね02");
//        Test ts = new Test();
//        ts.test();
//        try {
//            thread.sleep(150000);
//        } catch (InterruptedException e) { }
//        progressDialog.dismiss();
    }

    private class WebServer extends NanoHTTPD {
        public WebServer() {
            super(8080);
        }

        @Override
        public Response serve(IHTTPSession session) {

            Log.d("NanoHttod", "起動したよ！");
            String msg = "<!DOCTYPE html>\n" +
                    "<html lang=\"ja\">\n" +
                    "\n" +
                    "<head>\n" +
                    "    <meta charset=\"utf-8\">\n" +
                    "    <title>TM Lib : Editor</title>\n" +
                    "</head>\n" +
                    "\n" +
                    "<body>\n" +
                    "    <noscript>&lt;p class=\"warning-message\"&gt;&lt;a href=\"/web-app/support/javascript/\"&gt;JavaScriptを有効にする&lt;/a&gt;と、サンプルを実行できます。&lt;/p&gt;</noscript>\n" +
                    "\n" +
                    "    説明:txtarea内に好きな言葉を記述しダウンロードを行うとそのtxtファイルが記述されます\n" +
                    "<a href=\"./file/myfile.pdf\" download=\"myfile.pdf\">PDFファイル</a>" +
                    "    <div class=\"example-box\">\n" +
                    "        <textarea style=\"display: block\" rows=10 cols=50>Hello, World!</textarea><a href=\"jtavascript:;\" download=\"sample.txt\">ダウンロード</a>\n" +
                    "    </div>\n" +
                    "    <script type=\"text/javascript\">\n" +
                    "    (function() {\n" +
                    "        var anchor = document.links[document.links.length - 1]\n" +
                    "        if ('download' in anchor) {\n" +
                    "            anchor.download = 'sample.txt';\n" +
                    "            anchor.onclick = function() {\n" +
                    "                var text = this.previousSibling.value\n" +
                    "                this.href = 'data:,' + encodeURIComponent(text);\n" +
                    "            }\n" +
                    "        } else {\n" +
                    "            var message = document.createElement('span');\n" +
                    "            message.className = 'warning-message';\n" +
                    "            message.appendChild(document.createTextNode('お使いのブラウザでは、download属性がサポートされていません。'));\n" +
                    "\n" +
                    "            anchor.parentNode.appendChild(message);\n" +
                    "\n" +
                    "        }\n" +
                    "    })();\n" +
                    "    </script>\n" +
                    "</body>\n" +
                    "\n" +
                    "</html>\n" +
                    "\n";
            Map<String, String> parms = session.getParms();
            if (parms.get("username") == null) {
                msg += "<form action='?' method='get'>\n  <p>Your name: <input type='text' name='username'></p>\n" + "</form>\n";
            } else {
                msg += "<p>Hello, " + parms.get("username") + "!</p>";
                //Bluetoothが対応及びONになっているかをしらべる
                // Indicate scanning in the title
                // If we're already discovering, stop it
                if (mybluetoothAdapter01.isDiscovering()) {
                    mybluetoothAdapter01.cancelDiscovery();
                }
                // Request discover from BluetoothAdapter
                mybluetoothAdapter01.startDiscovery();
            }
            return newFixedLengthResponse(msg + "</body></html>\n");
        }
    }
}
